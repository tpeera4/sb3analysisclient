package analysis.service;
import static org.junit.Assert.assertNotNull;

import java.net.URISyntaxException;

import org.junit.Test;

import analysis.ast.Program;
import analysis.parsing.scratch3.BlockParsingException;
import analysis.parsing.scratch3.Scratch3XMLParser;
import analysis.parsing.scratch3.XMLParsingException;
import analysis.smell.LongScriptAnalysis;
import analysis.utils.SB3XMLConverter;

public class AnalysisServiceTest {

	@Test
	public void test() throws Exception {
		String testProjectID = "149974792";
		String projectSrc = new SB3XMLConverter().getSB3ProjectInXML(testProjectID);
		assertNotNull(projectSrc);
		Program program = Scratch3XMLParser.parseProgram(projectSrc);
		assertNotNull(program);
		LongScriptAnalysis analysis = new LongScriptAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String fullJsonReport = analysis.getFullReportAsJson();
		assertNotNull(fullJsonReport);
		assertNotNull(analysis.getShortReportAsJson());
		System.out.println(fullJsonReport);
	}

}
